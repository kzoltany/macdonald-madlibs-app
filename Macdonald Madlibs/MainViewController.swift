//
//  ViewController.swift
//  Macdonald Madlibs
//
//  Created by Charles Benedict on 1/24/15.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
  
  
  @IBOutlet weak var adjField: UITextField!
  @IBOutlet weak var placeField: UITextField!
  @IBOutlet weak var animalField: UITextField!
  @IBOutlet weak var soundField: UITextField!
  
  @IBAction func saveInfo(sender: UIButton) {
    txtField.adj = adjField.text
    txtField.place = placeField.text
    txtField.animal = animalField.text
    txtField.sound = soundField.text
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


}

