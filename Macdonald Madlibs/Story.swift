//
//  Story.swift
//  Macdonald Madlibs
//
//  Created by Charles Benedict on 1/24/15.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import Foundation

let story = Story()

class Story {
  let story = "\(txtField.adj) Macdonald had a \(txtField.place), E-I-E-I-O, and on that \(txtField.place) he had a \(txtField.animal), E-I-E-I-O, with a \(txtField.sound) \(txtField.sound) here, and a \(txtField.sound) \(txtField.sound) there, here an \(txtField.sound), there an \(txtField.sound), everywhere an \(txtField.sound) \(txtField.sound), \(txtField.adj) Macdonald had a \(txtField.place), E-I-E-I-O"
}