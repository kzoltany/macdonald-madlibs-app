//
//  Fields.swift
//  Macdonald Madlibs
//
//  Created by Charles Benedict on 1/24/15.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import Foundation

let txtField = Fields()

class Fields {
  var adj: String = ""
  var place: String = ""
  var animal: String = ""
  var sound: String = ""
}