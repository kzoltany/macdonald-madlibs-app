//
//  StoryViewController.swift
//  Macdonald Madlibs
//
//  Created by Charles Benedict on 1/24/15.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

import UIKit

class StoryViewController: UIViewController {

  @IBOutlet weak var storyLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    storyLabel.text = story.story

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  

  /*
  // MARK: - Navigation

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
      // Get the new view controller using segue.destinationViewController.
      // Pass the selected object to the new view controller.
  }
  */

}
